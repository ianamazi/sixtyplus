$(document).ready(function() {
	$.each($('.tags-single').prev('input').val().split(', '), function(index, itm) {
		addTag(itm, true);
	});
	$('body').on('click', '.tag-remove', function(e) {
		removeTag($(this).html());
		$(this).remove();
		e.preventDefault();
	});
	$('.tags-single').keypress(function (e) {
		if (e.which != 13) return;
		$txt = $(this).prev('input');
		addTag($(this).val(), false);
		e.preventDefault();
	});
});

function addTag(val, loading)
{
	$('.tags-list').html($('.tags-list').html() 
		+ '<a href="#" class="tag-remove">' + val + '</a> ');
	if (loading) return;
	$txt = $('.tags-single').prev('input');
	$txt.val($txt.val() + ($txt.val() != '' ? ', ' : '') + val);
	$('.tags-single').val('');
}
function removeTag(val)
{
	$txt = $('.tags-single').prev('input');
	var search1 = val + ', ';
	var search2 = ', ' + val;
	
	if ($txt.val().indexOf(search1) !== -1)
		$txt.val($txt.val().replace(search1, ''));
	else if ($txt.val().indexOf(search2) !== -1)
		$txt.val($txt.val().replace(search2, ''));
	else
		$txt.val($txt.val().replace(val, ''));
}
