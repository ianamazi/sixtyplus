<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * 
 * Manages which assets are upto date / need to be republished
 * Usage (In the extension's publishAssets() method)
 *   $force = AppPublisher::needsOverwrite('MbMenu');
 *   ...getAssetManager()->publish($dir, false, 0, $force)
*/

class AppPublisher
{
	private static $current;
	
	private static $latest = array(
		'MbMenu' => 1, //released: 1
		'dummy' => 1,
	);
	
	public static function needsOverwrite($what)
	{
		if (!isset(self::$latest[$what])) throw new Exception('Publisher has no version defined for ' . $what);
		$latest = self::$latest[$what];
	
		$cacheFile = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'publisherVersions.bin';
		if (self::$current == null)
			self::$current = file_exists($cacheFile) ? unserialize(@file_get_contents($cacheFile)) : array();
		
		$current = isset(self::$current[$what]) ? self::$current[$what] : 0;
		
		if ($yes = $latest > $current)
		{
			self::$current[$what] = $latest;
			@file_put_contents($cacheFile, serialize(self::$current));
		}
		
		return $yes;
	}
}
?>
