<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

//Enable php_openssl in php.ini - http://stackoverflow.com/questions/21962849/unable-to-find-the-socket-transport-ssl-did-you-forget-to-enable-it-when-you
//Make $_sequence protected in protected\extensions\yii-mail\vendors\swiftMailer\classes\Swift\ByteStream\AbstractFilterableInputStream.php

class AppMails
{
	public static function send($what, $model, $data = array())
	{
		$modelType = get_class($model);
		
		if ($what == 'contact')
		{
			$subject = sprintf('%s - %s - from %s', Yii::app()->name, $model->subject, $model->name);
			$body = self::getContent($what, array('msg' =>$model->body, 'name' => $model->name));
		}
		else if ($what == 'welcome')
		{
			$to = $model->email;
			$subject = sprintf('Welcome %s to %s', $model->name, Yii::app()->name);

			$emlData = array(
				'name' => $model->name,
				'url' => self::link('', Yii::app()->name),
				'login' => self::link('/site/login', 'login here'),
				'email' => $model->email,
				'password' => $data['password'],
			);

			$body = self::getContent($what, $emlData);
		}
		else if ($what == 'reset-password')
		{
			$to = $model->email;
			$subject = sprintf('Reset password at %s for %s', Yii::app()->name, $model->name);
			$body = self::getContent($what, array(
				'name' => $model->name,
				'email' => $model->email,
				'url' => self::link('site/reset/?token=' . $model->getResetToken(), 'this link'),
			));
		}
		else if ($what == 'reset-password-done')
		{
			$to = $model->email;
			$subject = sprintf('Password has been reset at %s for %s', Yii::app()->name, $model->name);
			$body = self::getContent($what, array(
				'name' => $model->name,
				'login' => self::link('/site/login', 'login here'),
				'email' => $model->email,
				'password' => $data['password'],
			));
		}
		else
		{
			throw new Exception('No Mail defined for type ' . $what);
		}

		self::sendMail($to, $subject, $body, Yii::app()->params['adminEmail']);
	}

	private static function sendMail($email, $subject, $message, $cc)
	{
		$test = Yii::app()->params['testEmail'];
		if ($test)
		{
			$message .= sprintf('*<b>NB</b>: To test, sending mail to "%s" instead of intended recipient %s', $test, $email);
			$email = $test;
		}

		self::sendSmtp($email, $subject, $message, $cc);
	}

	private static function sendSmtp($email, $subject, $message, $cc = false)
	{
		//$message = wordwrap($message, 120);
		$message = self::adjustBody($message);

		if (isset($_GET['previewemail'])) {
			$fmt = '<b>%s</b>: %s<br>' . PHP_EOL;
			echo '<div class="email">'
				//. sprintf($fmt, 'Template', ucwords(str_replace('-', ' ', self::$what)))
				. sprintf($fmt, 'From', Yii::app()->name . ' (' . Yii::app()->mail->transportOptions['username'] . ')')
				. sprintf($fmt, 'Subject', $subject)
				. $message . '</div>';
			//return;
		}

		// http://www.yiiframework.com/extension/mail/
		Yii::import('application.extensions.yii-mail.YiiMailMessage');
		$mail = new YiiMailMessage($subject, $message, 'text/html');
		$mail->addTo($email);
		if ($cc) { $mail->setCc($cc); $mail->setReplyTo($cc); }
		$mail->from = array(Yii::app()->mail->transportOptions['username'] => Yii::app()->name);
		Yii::app()->mail->send($mail);
	}

	public static function adjustBody($body)
	{
		return str_replace(PHP_EOL, "<br />" . PHP_EOL, $body);
	}

	private static function link($where, $txt = 'here')
	{
		return CHtml::link($txt, Yii::app()->createAbsoluteUrl($where));
	}

	private static function getContent($what, $data)
	{
		$data['signature'] = 'The ' . Yii::app()->name . ' Team<br/>'
			. CHtml::image(Yii::app()->createAbsoluteUrl('/css/email-logo.png'), 'plus65 logo', array('height'=> 50));

		$tpl = self::getTemplate($what);
		foreach ($data as $key=>$val)
		{
			$key = sprintf('{%s}', $key);
			$tpl = str_replace($key, $val, $tpl);
		}

		return $tpl;
	}

	private static function getTemplate($what)
	{
		$dir = Yii::app()->basePath . '/mail-templates/';
		$tpl = $dir . $what . '.html';
		if (!file_exists($tpl)) throw new Exception('Template ' . $tpl . ' not found');

		return file_get_contents($tpl);

		return Yii::app()->controller->site_file('protected/data/' . strtolower($what) . '.txt', 1);
	}
}
?>
