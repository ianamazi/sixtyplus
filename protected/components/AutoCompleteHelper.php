<?php
class AutoCompleteHelper {
	public static function registerJs($tbl, $field)
	{
		$date = self::overwriteJs($tbl, true);

		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile(Yii::app()->baseUrl . '/js/'.$tbl.'.js?date=' . $date);

		//TODO: Consider using widget
		$cs->registerScriptFile($cs->getCoreScriptUrl().'/jui/js/jquery-ui.min.js', CClientScript::POS_END);
		$cs->registerCssFile($cs->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');

		$cs->registerScript('ac-' . $field, 'jQuery("#' . $field . '").autocomplete({
		source: ' . $tbl . ',
		select: function(event, ui) {
		$link = $("#' . $field . '").next("a");
		$link.html(ui.item.label);
		$link.attr("href", "' . Yii::app()->baseUrl . '/locations/" + ui.item.value);
		return true; // Prevent the widget from inserting the value.
	},
});');
	}

	public static function overwriteJs($tbl, $return = false)
	{
		$date = Yii::app()->db->createCommand('select max(insert_date) from ' . $tbl)->queryScalar();
		$file = Yii::app()->basePath . '/../js/' . $tbl . '.js';

		//TODO:  Fix bug with time compare
		//echo filemtime($file) . PHP_EOL . strtotime($date);
		if ($return) return $date;
		if (file_exists($file) /*&& filemtime($file) > strtotime($date)*/) return;

		$op = 'var ' . $tbl . ' = [' . PHP_EOL;
		$items= Yii::app()->db->createCommand('select id, name from ' . $tbl . ' order by name')->queryAll();
		foreach ($items as $itm)
			$op .= sprintf('	{ label: "%s", value: %s },', $itm['name'], $itm['id']) . PHP_EOL;
		$op .= '];';
		file_put_contents($file, $op);
		return $date;
	}
}
?>