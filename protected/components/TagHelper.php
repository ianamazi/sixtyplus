<?php
class TagHelper {
	public static function addCriteria($dataProvider)
	{
		if (!isset($_GET['tag'])) return;
		$dataProvider->criteria = new CDbCriteria(array('condition' =>
			'tags like "%' . str_replace('-', ' ', $_GET['tag']) . '%"'));
	}

	public static function links($for, $tags = false)
	{
		if ($tags === false)
			$tags = self::getTags($for);
		else
			$tags = explode(', ', $tags);

		$op = array();
		$url = Yii::app()->baseUrl . '/' . $for . '/?tag=';

		foreach ($tags as $tag)
		{
			if ($tag == '') continue;
			$op[] = sprintf('<a href="%s%s">%s</a>', $url, str_replace(' ', '-', $tag), $tag);
		}

		return count($op) ? implode(' &nbsp; ' . PHP_EOL, $op) : 'None'; //NB: weird error that space forms part of link on web not local
	}

	public static function editor($for)
	{
		$tags = array();

		$cs = Yii::app()->getClientScript();

		$date = self::overwriteJs($for);
		$cs->registerScriptFile(Yii::app()->baseUrl . '/js/tags-editor.js');
		$cs->registerScriptFile(Yii::app()->baseUrl . '/js/tags-' .$for . '.js?date=' . $date);
		$cs->registerScript('ac-' . $for, 'jQuery(".tags-single").autocomplete({
	source: tags,
	select: function(event, ui) {
		addTag(ui.item.value);
		return false;
	},
});');

		return '
	<input type="text" class="tags-single" />
	<span class="info" title="Enter a tag in the textbox then hit enter"></span>
	<div class="tags-list"></div>' . PHP_EOL;
	}

	private static function getTags($for)
	{
		$file = Yii::app()->basePath . '/../js/tags-' . $for . '.js';
		if (!file_exists($file)) self::overwriteJs($for);
		$file = file_get_contents($file);
		$file = str_replace('",', '', str_replace(' "', '', $file));
		$items = explode(PHP_EOL, $file);
		unset($items[count($items) - 1]);
		unset($items[0]);
		return $items;
	}

	public static function overwriteJs($for, $force = false)
	{
		$file = Yii::app()->basePath . '/../js/tags-' . $for . '.js';
		if (file_exists($file) && !$force) return filemtime($file);

		$op = 'var tags = [' . PHP_EOL;
		$items = array();
		$rows= Yii::app()->db->createCommand('select tags from ' . $for)->queryAll();
		foreach ($rows as $row)
		{
			$tags = explode(', ', $row['tags']);
			foreach ($tags as $tag)
			{
				if ($tag == '' || array_search($tag, $items) !== false)
					continue;
				$items[] = $tag;
			}
		}
		sort($items);

		foreach ($items as $itm)
			$op.= ' "'.$itm.'",' . PHP_EOL;

		$op .= '];';
		file_put_contents($file, $op);

		return filemtime($file);
	}
}
?>
