<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private static $admins = array(
		'admin'=> 'sixxty15',
		'naidu' => 'sixxty',
	);

	private static $ids = array(
		'admin'=> 3,
		'naidu' => 2,
	);

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		if (isset(self::$admins[$this->username]))
		{
			$ok = self::$admins[$this->username] == $this->password;
			if (!$ok) $this->errorCode = self::ERROR_PASSWORD_INVALID;
		}
		else
		{
			$usr = Person::model()->findByAttributes(array('email' => $this->username));
			$ok = $usr && $usr->password == md5($this->password);
			if (!$ok)
			{
				$this->errorCode = $usr == null ? self::ERROR_USERNAME_INVALID : self::ERROR_PASSWORD_INVALID;
			}
		}

		if ($ok) $this->errorCode=self::ERROR_NONE;
		return !$this->errorCode;
	}

	public static function context($key)
	{
		if (Yii::app()->user->isGuest) return false;

		$name = Yii::app()->user->name;
		if ($key == 'admin') return isset(self::$admins[$name]);
		if ($key == 'superadmin') return $name == 'admin';
		if ($key == 'id') return isset(self::$admins[$name]) ? self::$ids[$name] :
			Person::model()->findByAttributes(array('name' => Yii::app()->user->name))->id;
	}
}
