/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` char(1) NOT NULL DEFAULT 'M',
  `location_id` int(20) DEFAULT NOT NULL,
  `contact` varchar(1024) DEFAULT NULL,
  `tags` varchar(1024) DEFAULT NULL,
  `about` varchar(4096) DEFAULT NULL,
  `points` int(10) DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `insert_by` varchar(100) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `latitude` decimal(9,6) DEFAULT NULL,
  `longitude` decimal(9,6) DEFAULT NULL,
  `insert_date` datetime NOT NULL,
  `insert_by` varchar(100) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `stays` */

DROP TABLE IF EXISTS `stays`;

CREATE TABLE `stays` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(20) NOT NULL,
  `location_id` int(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `on` datetime DEFAULT NULL,
  `notes` varchar(1024) DEFAULT NULL,
  `insert_date` datetime NOT NULL,
  `insert_by` varchar(100) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `organizations` */

DROP TABLE IF EXISTS `organizations`;

CREATE TABLE `organizations` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `location_id` int(20) NOT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `contact` varchar(1024) DEFAULT NULL,
  `tags` varchar(1024) DEFAULT NULL,
  `about` varchar(4096) DEFAULT NULL,
  `insert_date` datetime NOT NULL,
  `insert_by` varchar(100) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `interactions` */

DROP TABLE IF EXISTS `interactions`;

CREATE TABLE `interactions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(20) NOT NULL,
  `organization_id` int(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `on` datetime DEFAULT NULL,
  `notes` varchar(256) DEFAULT NULL,
  `insert_date` datetime NOT NULL,
  `insert_by` varchar(100) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `family` */

DROP TABLE IF EXISTS `family`;

CREATE TABLE `family` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(20) NOT NULL,
  `family_info` varchar(4096) NOT NULL,
  `insert_date` datetime NOT NULL,
  `insert_by` varchar(100) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `points` */

DROP TABLE IF EXISTS `points`;

CREATE TABLE `points` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(20) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `points` int(10) NOT NULL,
  `reason` varchar(256) NOT NULL,
  `insert_date` datetime NOT NULL,
  `insert_by` varchar(100) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
