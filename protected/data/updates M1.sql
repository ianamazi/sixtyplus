/*
11 June 2015 - auditing
people
locations
stays
organizations
interactions
family
points

see: gen.sql and D:\Imran\apps\Data\Gen Library\plus65.gen
*/

alter table points add column
  `insert_date` datetime NOT NULL;
alter table points add column
  `insert_by` varchar(100) NOT NULL;
alter table points add column
  `update_date` datetime NOT NULL;
alter table points add column
  `update_by` varchar(100) NOT NULL;

ALTER TABLE `{0}` CHANGE COLUMN `insertby` `insert_by` varchar(100) NOT NULL;
ALTER TABLE `{0}` CHANGE COLUMN `updateby` `update_by` varchar(100) NOT NULL;
update {0} set insert_date = now(), update_date = now(), insert_by = 'admin', update_by = 'admin';

alter table people add column
  `location_id` int(20) NOT NULL after gender;
update people set location_id = 3;
alter table people add column
  `tags` varchar(1024) NULL after contact;
alter table organizations add column
  `tags` varchar(1024) NULL after contact;

alter table people add column
  `email` varchar(100) NOT NULL after title;
alter table people add column
  `password` varchar(100) NOT NULL after email;
update people set password = md5('sixxty');

alter table people change column
  `password` `password` varchar(100) NOT NULL;
