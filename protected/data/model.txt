people
id name title dob gender contact about points
contact fields: skype phone email twitter facebook instagram


locations
id name type parent_id latitude longitude
type: country state city area


stays
person_id location_id type from to on notes
type: live study work visit born died


organizations
name type location_id parent_id about contact
type: education company club association


interactions
person_id organization_id type from to on notes
type: study work volunteer recognized event member


family
person_id family_info


points
person_id user_name date points reason
