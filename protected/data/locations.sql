/*Data for the table `locations` */

insert  into `locations`(`id`,`name`,`type`,`parent_id`,`latitude`,`longitude`) 
values (1,'India','country',NULL,'28.630694','77.223726')
,(2,'Tamil Nadu','state',1,'11.116017','78.365407')
,(3,'Chennai','city',2,'13.092452','80.222096')
,(4,'Coimbatore','city',2,'11.027548','76.968608')
,(5,'Maharashtra','state',1,'19.306356','75.495067')
,(6,'Bombay','city',5,'19.078091','72.968212');
