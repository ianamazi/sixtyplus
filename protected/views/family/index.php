<?php
/* @var $this FamilyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Families',
);

if (UserIdentity::context('admin'))
$this->menu=array(
	array('label'=>'Create Family', 'url'=>array('create')),
);
?>

<h1>Families</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
