<?php
/* @var $this FamilyController */
/* @var $data Family */
?>

<div class="view">

	<?php if (UserIdentity::context('admin'))
		echo CHtml::link('...' , array('update', 'id'=>$data->id), array('class' => 'right')); ?>

	<b>Family of</b> <?php echo CHtml::link($data->person->name, array('view', 'id'=>$data->id)); ?>

	<div class="family-info about"><?php echo CHtml::encode($data->family_info); ?></div>

</div>