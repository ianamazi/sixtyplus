<?php
$title = $this->action->Id == 'reset' ? 'Reset Password' : 'Login';
$this->pageTitle=Yii::app()->name . ' - ' . $title;
$this->breadcrumbs=array(
	$title,
);
?>

<h1><?php echo $title; ?></h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<?php if (!isset($mode) || $mode == 'password') { ?><div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<?php } if (!isset($mode)) { ?><p class="hint">Please <?php echo CHtml::link('reset', array('/site/reset')); ?> your password if you have forgotten it</p><?php } ?>

	<?php if (!isset($mode)) { ?><div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div><?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($title); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
