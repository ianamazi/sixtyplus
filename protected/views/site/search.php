<?php
$this->breadcrumbs=array(
	'Search',
);
?>


<h1>People</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($people),
	'itemView'=>'/people/_view',
)); ?>
<br><br><br>

<h1>Organizations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($organizations),
	'itemView'=>'/organizations/_view',
)); ?>
