<h1>Registering with Us</h1>

<p>We collect a one time subscription of &#8377;250/-</p>

<p>You can contact us at +91 98406 02325 to arrange to remit the same.</p>

<p>Enter your details in this
<a href="../docs/plus65-registration.doc">Word File</a> and email it to us at: 
<a href="mailto:submissions@plus65.in?subject=sixty plus registration [name]" target="_blank">submissions@plus65.in</a></p>

<div class="form">
	<p class="hint">Kindly note that we accept submissions for people who are dead, hence 
died in location is one kind of input we accept - in case you are taken back by this.</p>
</div>
