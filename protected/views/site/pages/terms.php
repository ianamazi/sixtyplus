<h1>Terms of Use</h1>

<ul>
	<li>The submitter is responsible for the accuracy of the information provided.</li>
	<li>We reserve the right to remove any profile / shut down the website at any time.
	Subscriptions will not be returned in this event</li>
	<li>We are not responsible if content submitted here becomes indexed/copied/linked to in other websites on the internet against your wishes.</li>
	<li>We are not liable for unwanted persons contacting you unnecessarily because you chose to provide contact information.</li>
</ul>
