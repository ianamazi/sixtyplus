<?php
$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<h1>About our Team</h1>

<?php $p = Person::model()->findByPk(1); ?>
<h2><?php echo CHtml::link($p->title . ' ' . $p->name, array('/people/' . $p->id)); ?></h2>
<i>Principal Promoter</i>
<p><?php echo $p->about; ?></p>

<?php $p = Person::model()->findByPk(2); ?>
<h2><?php echo CHtml::link($p->title . ' ' . $p->name, array('/people/' . $p->id)); ?></h2>
<i>Founder</i>
<p><?php echo $p->about; ?></p>

<h2><a href="http://cselian.com" target="_blank">Imran</a></h2>
<i>Lead Developer</i>
<p>A developer with 15 years experience, Imran wrote this website in just 2 days.</p>

<h1>History</h1>
<ul>
<li>June 2015 - Started development</li>
</ul>
