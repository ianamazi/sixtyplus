<?php
/* @var $this LocationsController */
/* @var $model Location */

$this->breadcrumbs=array(
	'Locations'=>array('index'),
	$model->name,
);

if (UserIdentity::context('admin'))
$this->menu=array(
	array('label'=>'List Location', 'url'=>array('index')),
	array('label'=>'Create Location', 'url'=>array('create')),
	array('label'=>'Update Location', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Location', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Location', 'url'=>array('admin')),
);
?>

<h1>View Location #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array_merge(array(
		'id',
		'name',
		array('label'=>$model->getAttributeLabel('type'), 'type'=>'raw', 'value'=>$model->type()),
		array('label'=>'Parent', 'type'=>'raw', 'value'=>$model->parentLocation()),
		array('label'=>'Location', 'type'=>'raw', 'value'=>$model->getCoords()),
		array('label'=>'Children', 'type'=>'raw',
			'value'=> $children . (UserIdentity::context('admin') ? ', ' . $model->link('create') . ', ' . $model->link('createorg') : '')),
		),
		$model->auditView()
	),
)); ?>
