<?php
/* @var $this LocationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Locations',
);

if (UserIdentity::context('admin'))
$this->menu=array(
	array('label'=>'Create Location', 'url'=>array('create')),
	array('label'=>'Manage Location', 'url'=>array('admin')),
);
?>

<h1>Locations</h1>

<ul class="close line-space">
<?php
$admin = UserIdentity::context('admin');
foreach ($items as $itm) show($admin, $itm);
function show($admin, $itm)
{
	$link = $admin ? sprintf('<span class="right"><a href="%s/locations/create/?parent=%s">add</a>
		<a href="%s/locations/update/%s">&hellip;</a></span>',
		Yii::app()->baseUrl, $itm['obj']['id'], Yii::app()->baseUrl, $itm['obj']['id']) : '';
	echo sprintf('<li>%s<a class="view" href="%s/locations/%s">%s</a></li>', 
		$link, Yii::app()->baseUrl, $itm['obj']['id'], $itm['obj']['name']);
	if (count($itm['items']))
	{
		echo "<ul>" . PHP_EOL;
		foreach ($itm['items'] as $itm) show($admin, $itm);
		echo "</ul>" . PHP_EOL;
	}
}
?>
</ul>
