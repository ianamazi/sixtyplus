<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/site.css" />

	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" />;

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo Yii::app()->name . '<small>, ' . Yii::app()->params['byline'] . '</small>'; ?></div>
	</div><!-- header -->

	<div id="mainMbMenu" class="nav popout">
		<form action="<?php echo Yii::app()->baseUrl; ?>/site/search" style="float: right; margin: 3px 20px 0 0;">
			<input type="text" name="s" placeholder="search" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>" />
		</form>
		<?php $this->widget('application.extensions.mbmenu.MbMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>Yii::app()->baseUrl, 'items' => array(
					array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
					array('label'=>'Register', 'url'=>array('/site/page', 'view'=>'register')),
					array('label'=>'Terms', 'url'=>array('/site/page', 'view'=>'terms')),
					array('label'=>'Contact', 'url'=>array('/site/contact/')),
				)),
				array('label'=>'Administer', 'visible'=>AppEnv::get('gii', 'CodeGen', 0), 'items'=> array(
					array('label'=>'Generator', 'url'=>array('/gii'), 'visible' => AppEnv::get('gii', 'CodeGen', 0), 'linkOptions' => array('target' => 'new')),
					//array('label'=>'Site', 'url'=>array('/site/page', 'view'=>'admin')),
				)),
				array('label'=>'People', 'url'=>array('/people/'), 'items'=> array(
					array('label'=>'Tags', 'url'=>array('/people/tags')),
				)),
				array('label'=>'Organizations', 'url'=>array('/organizations/'), 'items'=> array(
					array('label'=>'Tags', 'url'=>array('/organizations/tags')),
				)),
				array('label'=>'Locations', 'url'=>array('/locations/')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest, 'items' => array(
					array('label'=>'Reset Password', 'url'=>array('/site/reset')),
				)),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest, 'items' => array(
					array('label'=>'Update Profile', 'url'=>array('/people/update')),
				)),
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<span style="float: right;">Powered by <a href="http://www.yiiframework.com/" target="_blank">Yii Framework</a>,
			built by <a href="http://tg.cselian.com" target="_blank">cselian</a>.</span>
		Copyright &copy; <?php echo date('Y'); ?> <?php echo CHtml::link('Team Sixty Five Plus', array('/site/page', 'view' => 'about')); ?>.
		You agree to our <?php echo CHtml::link('Terms', array('/site/page', 'view' => 'terms')); ?>.
		
	</div><!-- footer -->
</div><!-- page -->

</body>
</html>
