<?php
/* @var $this OrganizationsController */
/* @var $dataProvider CActiveDataProvider */

if (isset($_GET['tag']))
$this->breadcrumbs=array(
	'Organizations'=>array('index'),
	'Tag: ' . $_GET['tag'],
);
else
$this->breadcrumbs=array(
	'Organizations',
);

if (UserIdentity::context('admin'))
$this->menu=array(
	array('label'=>'Create Organization', 'url'=>array('create')),
	array('label'=>'Manage Organization', 'url'=>array('admin')),
);
?>

<h1>Organizations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
