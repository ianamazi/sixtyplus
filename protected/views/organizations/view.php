<?php
/* @var $this OrganizationsController */
/* @var $model Organization */

$this->breadcrumbs=array(
	'Organizations'=>array('index'),
	$model->name,
);

if (UserIdentity::context('admin'))
$this->menu=array(
	array('label'=>'List Organization', 'url'=>array('index')),
	array('label'=>'Create Organization', 'url'=>array('create')),
	array('label'=>'Update Organization', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Organization', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Organization', 'url'=>array('admin')),
);
?>

<h1>View Organization #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array_merge(array(
		'id',
		'name',
		'type',
		array('label'=>$model->getAttributeLabel('location_id'), 'type'=>'raw', 'value'=>$model->location->link()),
		array('label'=>$model->getAttributeLabel('parent_id'), 'type'=>'raw', 'value'=>$model->parentLink()),
		array('label'=>$model->getAttributeLabel('contact'), 'type'=>'raw', 'value'=>$model->contact()),
		array('label'=>$model->getAttributeLabel('tags'), 'type'=>'raw', 'value'=>$model->tagLinks()),
		array('label'=>$model->getAttributeLabel('about'), 'type'=>'raw', 'value'=>$model->about),
		),
		$model->auditView()
	),
)); ?>
