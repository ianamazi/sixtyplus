<?php
/* @var $this OrganizationsController */
/* @var $data Organization */
?>

<div class="view">

	<?php if (UserIdentity::context('admin'))
		echo CHtml::link('...' , array('update', 'id'=>$data->id), array('class' => 'right')); ?>

	<?php echo CHtml::link($data->name, array('view', 'id'=>$data->id)); ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo $data->type(); ?>,

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_id')); ?>:</b>
	<?php echo $data->location->link(); ?>,

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_id')); ?>:</b>
	<?php echo $data->parent_id == '' ? '' : $data->parentOrganization->link(); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tags')); ?>:</b>
	<?php echo $data->tagLinks(); ?>,

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact')); ?>:</b>
	<?php echo $data->contact(); ?>
	<br />

	<div class="about">
	<?php echo $data->about; ?>
	</div>

</div>