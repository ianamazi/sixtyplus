<?php
/* @var $this PeopleController */
/* @var $model Person */

$this->breadcrumbs=array(
	'People'=>array('index'),
	$model->name,
);

if (UserIdentity::context('admin'))
$this->menu=array(
	array('label'=>'List Person', 'url'=>array('index')),
	array('label'=>'Create Person', 'url'=>array('create')),
	array('label'=>'Update Person', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Person', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Person', 'url'=>array('admin')),
);
?>

<h1>View Person #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array_merge(array(
		'id',
		'name',
		'title',
		array('label'=>'Picture', 'type'=>'raw', 'value'=>$model->getPicture()),
		array('label'=>$model->getAttributeLabel('email'), 'type'=>'raw', 'value'=>$model->email, 'visible'=>UserIdentity::context('admin')),
		array('label'=>$model->getAttributeLabel('dob'), 'type'=>'raw', 'value'=>Formatter::date($model->dob)),
		array('label'=>$model->getAttributeLabel('gender'), 'type'=>'raw', 'value'=>$model->gender()),
		array('label'=>$model->getAttributeLabel('location_id'), 'type'=>'raw', 'value'=>$model->location->link()),
		array('label'=>$model->getAttributeLabel('contact'), 'type'=>'raw', 'value'=>$model->contact()),
		array('label'=>$model->getAttributeLabel('tags'), 'type'=>'raw', 'value'=>$model->tagLinks()),
		array('label'=>$model->getAttributeLabel('about'), 'type'=>'raw', 'value'=>$model->about),
		'points',
		array('label'=>'Family', 'type'=>'raw', 'value'=>$model->familyLink()),
		),
		$model->auditView()
	),
)); ?>

<br><br>
<h2>Organizations (Interactions)</h2>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($interactions, array('pagination' => false)),
	'itemView'=>'/interactions/_view',
	'template'=>'{items}',
)); ?>

<?php if (UserIdentity::context('admin') || UserIdentity::context('id') == $model->id) { ?>
<br><br>
<input type="button" value="add organizations" class="interaction-form" onclick="showForm(this)" />
<div class="sub-form interaction-form form" style="display: none;">
<p class="hint">NB: You can change the details and submit this form multiple times</p>
<?php $this->renderPartial('/interactions/_form', array('model'=>$newInteraction)); ?>
</div>
<?php } ?>

<br><br>
<h2>Locations (Stays / Visits)</h2>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($stays, array('pagination' => false)),
	'itemView'=>'/stays/_view',
	'template'=>'{items}',
)); ?>

<?php if (UserIdentity::context('admin') || UserIdentity::context('id') == $model->id) { ?>
<br><br>
<input type="button" value="add locations" class="stay-form" onclick="showForm(this)" />
<div class="sub-form stay-form form" style="display: none;">
<p class="hint">NB: You can change the details and submit this form multiple times</p>
<?php $this->renderPartial('/stays/_form', array('model'=>$newStay)); ?>
</div>
<?php } ?>

<script type="text/javascript">
function showForm(btn)
{
	btn = $(btn);
	$id = '.' + btn.attr('class');
	$($id).last().toggle();
}
</script>
