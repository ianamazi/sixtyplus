<?php
/* @var $this PeopleController */
/* @var $data Person */
?>

<div class="view">

	<div class="right" style="margin-left: 10px"><?php echo $data->getPicture(); ?></div>
	<?php echo CHtml::link($data->title . ' ' . $data->name, array('view', 'id'=>$data->id)); ?>,

	<?php if (UserIdentity::context('admin'))
		echo CHtml::link('...' , array('update', 'id'=>$data->id), array('class' => 'right')); ?>

	<?php echo $data->gender(); ?> living in 
	<?php echo $data->location->link(); ?>,

	<b><?php echo CHtml::encode($data->getAttributeLabel('dob')); ?>:</b>
	<?php echo Formatter::date($data->dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tags')); ?>:</b>
	<?php echo $data->tagLinks(); ?>,

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact')); ?>:</b>
	<?php echo $data->contact(); ?>
	<br />

	<div class="about">
	<?php echo $data->about; ?>
	</div>
	
	<div style="clear: both;height: 0px;"></div>
</div>