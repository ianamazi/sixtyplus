<?php
/* @var $this PeopleController */
/* @var $dataProvider CActiveDataProvider */

if (isset($_GET['tag']))
$this->breadcrumbs=array(
	'People'=>array('index'),
	'Tag: ' . $_GET['tag'],
);
else
$this->breadcrumbs=array(
	'People',
);

if (UserIdentity::context('admin'))
$this->menu=array(
	array('label'=>'Create Person', 'url'=>array('create')),
	array('label'=>'Manage Person', 'url'=>array('admin')),
);
?>

<h1>People</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
