<?php
/* @var $this StaysController */
/* @var $data Stay */
?>

	<?php if (UserIdentity::context('admin'))
		echo CHtml::link('...' , array('/stays/update', 'id'=>$data->id), array('class' => 'right', 'target' => '_blank')); ?>

	<?php echo $data->type(); ?>
	in <?php echo $data->location->link(); ?>

	<?php $data->txnDate('from'); ?>
	<?php $data->txnDate('to'); ?>
	<?php $data->txnDate('on', true); ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />
