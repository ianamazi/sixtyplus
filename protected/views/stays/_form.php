<?php
/* @var $this StaysController */
/* @var $model Stay */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stay-form',
	'action'=> $model->isNewRecord ? Yii::app()->baseUrl . '/stays/create' : '',
	'htmlOptions' => array('target'=> $model->isNewRecord ? '_blank' : ''),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php if (!$model->isNewRecord) { ?><div class="row">
		<?php echo $form->labelEx($model,'person_id'); ?>
		<?php echo $form->textField($model,'person_id'); ?>
		<?php echo $model->person_id != '' ? $model->person->link() : ''; ?>
		<?php echo $form->error($model,'person_id'); ?>
	</div><?php } else { echo $form->hiddenField($model,'person_id'); } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'location_id'); ?>
		<?php echo $form->textField($model,'location_id'); ?>
		<?php echo $model->location_id != '' ? $model->location->link() : '<a target="_blank"></a>'; ?>
		<?php AutoCompleteHelper::registerJs('locations', 'Stay_location_id') ?>
		<?php echo $form->error($model,'location_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->radioButtonList($model,'type', Stay::$types, array('separator' => '')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from'); ?>
		<?php echo Formatter::datePicker($this, $model,'from'); ?>
		<?php echo $form->error($model,'from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to'); ?>
		<?php echo Formatter::datePicker($this, $model,'to'); ?>
		<?php echo $form->error($model,'to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'on'); ?>
		<?php echo Formatter::datePicker($this, $model,'on'); ?>
		<?php echo $form->error($model,'on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->