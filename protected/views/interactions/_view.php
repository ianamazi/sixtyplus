<?php
/* @var $this InteractionsController */
/* @var $data Interaction */
?>

	<?php if (UserIdentity::context('admin'))
		echo CHtml::link('...' , array('/interactions/update', 'id'=>$data->id), array('class' => 'right', 'target' => '_blank')); ?>

	<?php echo $data->type(); ?>
	in <?php echo $data->organization->link(); ?>

	<?php $data->txnDate('from'); ?>
	<?php $data->txnDate('to'); ?>
	<?php $data->txnDate('on', true); ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />
