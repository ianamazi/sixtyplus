<?php
/* @var $this InteractionsController */
/* @var $model Interaction */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'interaction-form',
	'action'=> $model->isNewRecord ? Yii::app()->baseUrl . '/interactions/create' : '',
	'htmlOptions' => array('target'=> $model->isNewRecord ? '_blank' : ''),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php if (!$model->isNewRecord) { ?><div class="row">
		<?php echo $form->labelEx($model,'person_id'); ?>
		<?php echo $form->textField($model,'person_id'); ?>
		<?php echo $model->person_id != '' ? $model->person->link() : ''; ?>
		<?php echo $form->error($model,'person_id'); ?>
	</div><?php } else { echo $form->hiddenField($model,'person_id'); } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'organization_id'); ?>
		<?php echo $form->textField($model,'organization_id'); ?>
		<?php echo $model->organization_id != '' ? $model->organization->link() : '<a target="_blank"></a>'; ?>
		<?php AutoCompleteHelper::registerJs('organizations', 'Interaction_organization_id') ?>
		<?php if (isset($_GET['orgCreated'])) echo '(Organization with type ' 
			. $model->organization->type . ' just created - '
			. CHtml::link('edit', array('organizations/update/' . $model->organization->id), array('target'=>'_blank')) . ')'; ?>
		<?php echo $form->error($model,'organization_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->radioButtonList($model,'type', Interaction::$types, array('separator' => '')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from'); ?>
		<?php echo Formatter::datePicker($this, $model,'from'); ?>
		<?php echo $form->error($model,'from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to'); ?>
		<?php echo Formatter::datePicker($this, $model,'to'); ?>
		<?php echo $form->error($model,'to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'on'); ?>
		<?php echo Formatter::datePicker($this, $model,'on'); ?>
		<?php echo $form->error($model,'on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->