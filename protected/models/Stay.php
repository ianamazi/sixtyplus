<?php

/**
 * This is the model class for table "stays".
 *
 * The followings are the available columns in table 'stays':
 * @property integer $id
 * @property integer $person_id
 * @property integer $location_id
 * @property string $type
 * @property string $from
 * @property string $to
 * @property string $on
 * @property string $notes
 */
class Stay extends ActiveRecord
{
	public static $types = array('live' => 'Lived', 'study' => 'Studied', 'work' => 'Worked',
		'visit' => 'Visited', 'born' => 'Was Born', 'Died' => 'Died', );

	public function type()
	{
		return $this->type == '' ? 'Not Set' : self::$types[$this->type];
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stays';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('person_id, location_id, type', 'required'),
			array('person_id, location_id', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>20),
			array('notes', 'length', 'max'=>1024),
			array('from, to, on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, person_id, location_id, type, from, to, on, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'person_id' => 'Person',
			'location_id' => 'Location',
			'type' => 'Type',
			'from' => 'From',
			'to' => 'To',
			'on' => 'On',
			'notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('to',$this->to,true);
		$criteria->compare('on',$this->on,true);
		$criteria->compare('notes',$this->notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
