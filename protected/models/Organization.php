<?php

/**
 * This is the model class for table "organizations".
 *
 * The followings are the available columns in table 'organizations':
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $location_id
 * @property integer $parent_id
 * @property string $contact
 * @property string $tags
 * @property string $about
 */
class Organization extends ActiveRecord
{
	public static $types = array('education' => 'Education', 'company' => 'Company', 'club' => 'Club', 'association' => 'Association');

	public function type()
	{
		return $this->type == '' ? 'Not Set' : self::$types[$this->type];
	}

	public function parentLink()
	{
		if ($this->parent_id == '') return null;
		return $this->parentOrganization->link();
	}

	public function link($what = false)
	{
		return sprintf('<a href="%s/organizations/%s" target="_blank">%s</a>', 
			Yii::app()->baseUrl, $this->id, $this->name);
	}

	public function tagLinks()
	{
		return TagHelper::links('organizations', $this->tags);
	}

	function contact()
	{
		if ($this->contact == '') return 'Unknown';
		return Formatter::contact($this->contact);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'organizations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, type, location_id', 'required'),
			array('location_id, parent_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('type', 'length', 'max'=>20),
			array('contact, tags', 'length', 'max'=>1024),
			array('about', 'length', 'max'=>4096),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, type, location_id, parent_id, contact, tags, about', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
			'parentOrganization' => array(self::BELONGS_TO, 'Organization', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'type' => 'Type',
			'location_id' => 'Location',
			'parent_id' => 'Parent',
			'contact' => 'Contact',
			'tags' => 'Tags',
			'about' => 'About',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('about',$this->about,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Organization the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
