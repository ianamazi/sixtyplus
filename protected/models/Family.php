<?php

/**
 * This is the model class for table "family".
 *
 * The followings are the available columns in table 'family':
 * @property integer $id
 * @property integer $person_id
 * @property string $family_info
 * @property string $insert_date
 * @property string $insert_by
 * @property string $update_date
 * @property string $update_by
 */
class Family extends ActiveRecord
{

	public function renderInfo($people = false, $name = false)
	{
		$sub = $people !== false;
		if (!$name) $name = $this->person->name;
		if (!$people) $people = self::parse($this->family_info);

		$father = false; $mother = false;
		$siblings = array();
		$rows = array();

		$thirdcol = $sub ? '' : '<td></td>';
		foreach ($people as $p)
		{
			$r = $p['relation'];
			if ($r == 'Father')
				$father = self::personTag($p);
			else if ($r == 'Mother')
				$mother = self::personTag($p);
			else if ($r == 'Brother' || $r == 'Sister')
				$siblings[] = self::personTag($p);
			else
				$rows[] = '<tr><td>' . $r . '</td><td>'
					. self::personTag($p)
					. '</td>' . (isset($p['sub']) ? '<td>' . $this->renderInfo($p['sub'], $p['name']) . '</td>' : '')
					. '</tr>' . PHP_EOL;
		}

		$tbl = '<table border="1">' . PHP_EOL;
		if ($father || $mother) $tbl .= "<tr><td>$father</td><td>$mother</td>$thirdcol</tr>" . PHP_EOL;
		$tbl .= "<tr><td colspan='2'><em>$name</em></td>$thirdcol</tr>" . PHP_EOL;
		if (count($siblings)) $tbl .= '<tr><td>Siblings</td><td>' . implode('<br/>', $siblings) . '</td>' . $thirdcol . '</tr>' . PHP_EOL;
		foreach ($rows as $row) $tbl .= $row;
		$tbl .= '</table>';
		return $tbl;
	}

	private static function personTag($p)
	{
		$male = array('Brother', 'Son', 'Husband', 'Father');
		$gender = array_search($p['relation'], $male) !== false ? 'male' : 'female';
		return '<span class="'.$gender.'" title="' . $p['relation'] . '">' . $p['name'] . '</span>';
	}

	private static function parse($text, $sub = false)
	{
		$lines = explode($sub ? ', ' : PHP_EOL, $text);
		$op = array();
		foreach ($lines as $line)
		{
			$bits = explode(': ', $line, 2);

			if ($pos = stripos($bits[1], ', ') !== false) {
				$comma = stripos($bits[1], ',');
				$sub = self::parse(substr($bits[1], $pos + $comma + 1), true);
				$bits[1] = substr($bits[1], 0, $pos + $comma - 1);
			} else {
				$sub = false;
			}

			$row = array('relation' => $bits[0], 'name' => $bits[1]);
			if ($sub) $row['sub'] = $sub;

			$op[] = $row;
		}
		return $op;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'family';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('person_id, family_info', 'required'),
			array('person_id', 'numerical', 'integerOnly'=>true),
			array('family_info', 'length', 'max'=>4096),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, person_id, family_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'person_id' => 'Person',
			'family_info' => 'Family Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('family_info',$this->family_info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Family the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
