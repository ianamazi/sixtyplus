<?php
class ActiveRecord extends CActiveRecord
{
	protected function beforeSave ()
	{
		$name = get_class($this);
		if ($name == 'Stay' || $name == 'Interaction')
		{
			$this->from = Formatter::dateForSql($this->from);
			$this->to = Formatter::dateForSql($this->to);
			$this->on = Formatter::dateForSql($this->on);
		}

		if ($this->isNewRecord) {
			$this->insert_date = new CDbExpression('NOW()');
			$this->insert_by = Yii::app()->user->name;
		}

		$this->update_date = new CDbExpression('NOW()');
		$this->update_by = Yii::app()->user->name;

		return parent::beforeSave();
	}

	public function auditView()
	{
		$vis = UserIdentity::context('admin');
		return array(
			array('label'=>'Insert Date', 'type'=>'raw', 'value'=>Formatter::date($this->insert_date), 'visible'=>$vis),
			array('label'=>'Insert By', 'type'=>'raw', 'value'=>$this->insert_by, 'visible'=>$vis),
			array('label'=>'Update Date', 'type'=>'raw', 'value'=>Formatter::date($this->update_date), 'visible'=>$vis),
			array('label'=>'Update By', 'type'=>'raw', 'value'=>$this->update_by, 'visible'=>$vis),
		);
	}
	
	function txnDate($field, $last = false)
	{
		if ($this->$field == '0000-00-00 00:00:00') return;
		echo sprintf('<b>%s:</b> %s%s', $this->getAttributeLabel($field),
			Formatter::date($this->$field), $last ? '' : ',');
	}
}
?>
