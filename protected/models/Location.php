<?php

/**
 * This is the model class for table "locations".
 *
 * The followings are the available columns in table 'locations':
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $parent_id
 * @property string $latitude
 * @property string $longitude
 */
class Location extends ActiveRecord
{
	public static $types = array('country' => 'Country', 'state' => 'State', 'city' => 'City', 'area' => 'Area');

	public function type()
	{
		return $this->type == '' ? 'Not Set' : self::$types[$this->type];
	}

	public function setType()
	{
		$parent = $this->parentLocation->type;
		$this->type = 'area';
		$last = '';
		foreach (self::$types as $type=>$name)
		{
			if ($last == $parent)
				$this->type = $type;
			$last = $type;
		}
	}

	public function parentLocation()
	{
		if ($this->parent_id == null) return 'None';
		return $this->parentLocation->link();
	}

	public function link($what = false)
	{
		if ($what == 'create') return sprintf('<a href="%s/locations/create/?parent=%s">%s</a>', 
			Yii::app()->baseUrl, $this->id, '+ add');

		if ($what == 'createorg') return sprintf('<a href="%s/organizations/create/?location=%s">%s</a>', 
			Yii::app()->baseUrl, $this->id, '+ add organization');

		return sprintf('<a href="%s/locations/%s" target="_blank">%s</a>', 
			Yii::app()->baseUrl, $this->id, $this->name);
	}

	public function getChildren($obj = false)
	{
		$list = Location::model()->findAllByAttributes(array('parent_id'=>$this->id), array('order' => 'name'));
		if ($obj) return $list;
		$op = array();
		foreach ($list as $l) $op[] = $l->link();
		return implode(', ',  $op);
	}

	public function getCoords()
	{
		//print_r($this);
		if ($this->latitude == 0 || $this->longitude == 0)
			return 'Not Set';

		return sprintf('<a href="http://maps.google.com/?z=8&q=%s,%s" target="_blank">%s, %s</a>',
			$this->latitude, $this->longitude, $this->latitude, $this->longitude);
	}

	public function adjustCoords($back = false)
	{
		if ($back)
		{
			if ($this->latitude == 0) return;
			$bits = explode(', ', $this->latitude);
			$this->latitude = $bits[0];
			$this->longitude = $bits[1];
			return;
		}

		if ($this->latitude == 0 || $this->longitude == 0)
			$this->latitude = '';
		else
			$this->latitude .= ', ' . $this->longitude;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'locations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, type', 'required'),
			array('parent_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('type', 'length', 'max'=>20),
			array('latitude, longitude', 'length', 'max'=>9),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, type, parent_id, latitude, longitude', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parentLocation' => array(self::BELONGS_TO, 'Location', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'type' => 'Type',
			'parent_id' => 'Parent',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
