<?php

/**
 * This is the model class for table "people".
 *
 * The followings are the available columns in table 'people':
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $email
 * @property string $password
 * @property string $dob
 * @property string $gender
 * @property integer $location_id
 * @property string $contact
 * @property string $tags
 * @property string $about
 * @property integer $points
 */
class Person extends ActiveRecord
{
	public static $genders = array('M'=>'Male','F'=>'Female');

	public function gender()
	{
		return $this->gender == '' ? 'Not Set' : self::$genders[$this->gender];
	}

	function contact()
	{
		if ($this->contact == '') return 'Declined to give';
		return Formatter::contact($this->contact);
	}

	public function link($what = false)
	{
		return sprintf('<a href="%s/people/%s">%s</a>', 
			Yii::app()->baseUrl, $this->id, $this->name);
	}
	
	public function familyLink()
	{
		$f = Family::model()->findByAttributes(array('person_id' => $this->id));
		return CHtml::link($f ? 'view' : 'create', array('/family/' . ($f ? $f->id : 'create/?person=' . $this->id)));
	}

	public function tagLinks()
	{
		return TagHelper::links('people', $this->tags);
	}
	
	public function savePicture($file)
	{
		$si = new SimpleImage();
		$si->load($file);
		$si->resizeToWidth(150);
		$si->save($this->getPicture(true));
	}

	public function getPicture($path = false)
	{
		$name = str_replace(' ', '-', $this->name);
		$file = Yii::app()->basePath . "/../img/people/$this->id-$name.jpg";
		if ($path === 'remove' && file_exists($file)) unlink($file);
		if ($path) return $file;
		$image = Yii::app()->getController()->action->getId() == 'index';
		if (file_exists($file) == false && !$image) return 'None';
		return CHtml::image(Yii::app()->baseUrl . '/img/people/'
			. (file_exists($file) ? $this->id . '-' . $name . '.jpg' : 'unknown.jpg')
			, $this->name);
	}

	public function generatePassword()
	{
		$length = 8;
		$strength = 0;
	// http://www.webtoolkit.info/php-random-password-generator.html
		$vowels = 'aeiou'; $consonants = 'bcdfghjklmnpqrstvwxzy';
		if ($strength & 1) $consonants .= '123456789';
		if ($strength & 2)  $consonants .= '@#$%';
		
		$password = '';
		$alt = 1;
		for ($i = 0; $i < $length; $i++)
		{
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		$this->password = md5($password);
		return $password;
	}

	public function getResetToken()
	{
		return base64_encode($this->email . '/' . $this->update_date);
	}

	public static function getByToken($token)
	{
		$token = explode('/', base64_decode($token));
		$model = self::model()->findByAttributes(array('email'=>$token[0]));
		if ($model == null || $model->update_date != $token[1])
			throw new Exception('Invalid token for ' . $token[0]);
		return $model;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'people';
	}

	protected function beforeSave ()
	{
		if($this->dob <> '')
		{
			$this->dob = Formatter::dateForSql($this->dob);
		}

		return parent::beforeSave();
	}

	protected function afterSave ()
	{
		if (count($_FILES) && $_FILES['picture']['tmp_name'] != '')
			$this->savePicture($_FILES['picture']['tmp_name']);
		else if (isset($_POST['remove-picture']))
			$this->getPicture('remove');

		return parent::beforeSave();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email, password, location_id', 'required'),
			array('location_id, points', 'numerical', 'integerOnly'=>true),
			array('name, email, password', 'length', 'max'=>100),
			array('title', 'length', 'max'=>20),
			array('gender', 'length', 'max'=>1),
			array('contact, tags', 'length', 'max'=>1024),
			array('about', 'length', 'max'=>4096),
			array('dob', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, title, email, dob, gender, location_id, contact, tags, about, points', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
		);
	}
	
	public function getStays()
	{
		return Stay::model()->findAllByAttributes(array('person_id' => $this->id));
	}

	public function getInteractions()
	{
		return Interaction::model()->findAllByAttributes(array('person_id' => $this->id));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'title' => 'Title',
			'email' => 'Email',
			'password' => 'Password',
			'dob' => 'Birthday',
			'gender' => 'Gender',
			'location_id' => 'Lives In',
			'contact' => 'Contact',
			'tags' => 'Tags',
			'about' => 'Biography',
			'points' => 'Points',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('points',$this->points);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
