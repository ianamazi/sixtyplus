<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$stats = $this->getCount('people') . '<br />' . $this->getCount('organizations') . '<br />' . $this->getCount('locations');

		$this->render('index',array('stats'=>$stats));
	}

	private function getCount($tbl)
	{
		$cnt = Yii::app()->db->createCommand('select count(*) from '.$tbl.'')->queryScalar();
		$base = Yii::app()->baseUrl;
		$fmt = '<a href="%s/%s/">%s %s</a>';
		return sprintf($fmt, $base, $tbl, $cnt, ucfirst($tbl));
	}

	public function actionSearch()
	{
		if (!isset($_GET['s']))
		{
			$this->renderText('A search query is necessary, please use the search box in the menu bar');
			return;
		}

		$s = $_GET['s'];
		$people = Person::model()->findAllBySql("select * from people where name like '%$s%' or about like '%$s%' or tags like '%$s%'");
		$orgs = Organization::model()->findAllBySql("select * from organizations where name like '%$s%' or about like '%$s%' or tags like '%$s%'");
		$this->render('search',array('people'=>$people,'organizations'=>$orgs));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		else
		{
			$model->rememberMe = true;
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	public function actionReset()
	{
		if (isset($_GET['token']))
		{
			$mode = 'password';
			$person = Person::getByToken($_GET['token']);
			$model = new LoginForm;
			$model->username = $person->email;
			if (isset($_POST['LoginForm']))
			{
				$password = $_POST['LoginForm']['password'];
				$person->password = md5($password);
				if (!$person->save())
					die(CHtml::errorSummary($person));

				AppMails::send('reset-password-done', $person, array('password'=>$password));
				$this->redirect('/site/login');
			}
		}
		else
		{
			$mode = 'username';
			$model = new LoginForm;
			if (isset($_POST['LoginForm']))
			{
				$email = $_POST['LoginForm']['username'];
				$model->username = $email;
				$person = Person::model()->findByAttributes(array('email'=>$email));
				if ($person == null) {
					$model->addError('username', 'Email not found');
				} else {
					AppMails::send('reset-password', $person);
					$this->renderText('An email has been sent to ' . $email);
					return;
				}
			}
		}

		$this->render('login',array('model'=>$model, 'mode'=>$mode));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}